# README

## Running the Model
1. Download the project onto your machine. 
2. Install any missing dependencies by referencing the included `fuelmix.yml` file in the project's home directory. You can either create a new python environment using your favorite package manager, or install the missing dependencies directly to your machine. For example, to create a new environment with the included dependencies using anaconda, you can run the following shell command:
    - `conda env create -f fuelmix.yml`
3. If you chose to create a new environment to run the model, be sure to activate it.
4. Run the model: simply run the file `main.py` with python 3 from the shell, using either its absolute or relative filepath, e.g.:
    - `$ python3 main.py` (from the file's own directory)
    - `$ python3 <relative-file-path>/main.py` (from any directory)
    - `$ python3 <absolute-file-path>/main.py` (from any directory)
    - Note that these commands are specific to the MacOS shell; depending on your machine, the command to run a python file may look slightly different.

## Methodology for Determining the Typical Day
The typical day was determined using a simple sum-of-squared-differences minimization procedure:
1. For each timestamp in each day in the August dataset, the difference in power generation between the given timestamp in the given day and the same timestamp in the average day was calculated.
2. These differences were then squared and added together, giving the sum-of-squared-differences.
3.  The day with the smallest sum-of-squared-differences was selected as the typical day, representing the day closest matching the average based on this difference calculation methodology.

## Key Insights and Analysis
- Looking at the heatmap data for the whole month of August, it is clear that power generation follows the day/night cycle of human activity closely, with significantly higher power generation, and correspondingly high CO2 emissions, during standard working/waking hours.
- Looking at the plotted curves of total NYISO generation and emissions for the average day against the typical day, it is clear that the highest inter-day variability is between the hours of 9:00am and 9:00pm (21:00), indicating that not only are power and emissions higher during expected periods of human activity, but also more unpredictable. This is evident by the wider (taller) error bars in the band surrounding the average day curve for this period of time, particularly in the period of approximately 12:00pm - 6:00pm. 
- Looking again at the plotted curves of total NYISO generation and emissions, the least inter-day variability is between the hours of 5:00am - 9:00am, and 9:00pm - 12:00am:
    - As these are periods before and after work for many people, combining this insight with the fact stated above that the highest inter-day variability is during traditional working hours, this would seem to indicate that the use of electricity in the home is more predictable, on average, than the use of electricity to power workplaces and their auxiliary facilities / services.
- Looking again at the heatmap data, days representing weekends show significantly lower power generation and emissions, and a slower ramp-up to their highest levels:
    - Weekend days are August (3, 4), (10, 11), (17, 18), (24, 25), and (31). 
    - August 17/18 are out-of-character for this trend during the main daylight hours, but still show the relatively low levels for the period of ~5:00-8:00am, compared to weekdays, matching the slow ramp-up evident in all weekend days in August. 
- In both the heatmap plot, and the plot of total generation curves, we can see that the emissions generated seem to track the power generation closely. This may indicate that the fuel-mix makeup of the generated power has a somewhat consistent distribution regardless of the amount of power generated. 

## Suggestions for Further Analysis and/or Model Improvement
- Principally, this model could benefit significantly from being expanded beyond the month of August 2019, to identify trends in larger time periods, as well as to identify what is significant about particular days and weeks in the month of August, as well as the month itself. 
    - It would be relatively simple to adapt the model I built to any given month of data, with a few tweaks to how the input data is processed, depending on the month. 
- The fact that power and emissions data seems to track traditional working hours so well would seem to indicate that the type of usage is predictably related to powering offices and business; most likely, the fundamental power usage of buildings that only operate during periods of human activity, for human use. There would be a strong argument for improving the efficiency of buildings if this were the case. I would bet that the majority of this usage is related to air conditioning, and that could be looked into by including temperature data alongside power and emissions data, and expanding the analysis well beyond the month of August where we would begin to see strong temperature trends.
- The fact that the typical day (August 17) falls outside the standard error of the mean band for the average day in the early morning hours may indicate something unique and predictable about this day, suggesting why it may have out-of-character demand during these hours. Each day in the month of August could be analysed as such to look for predictable events (and so, in principle, could any day in any month compared to its average, whether that be the month, the season, or some other justifiably-contiguous period of time).
- Although the total generation and emissions curves for the average day have a fairly smooth hump during the peak hours, the typical day curve shows no such smoothness. It would be interesting to see if there are peaks and troughs during this period for more days in August, and if it was a strong pattern, that could indicate something predictable about power demand during the day. 
- A dip in the total power generation curve may not necessarily represent a dip in demand -- it could also be that power demand is simply more predictable during that period, which could result in the NYISO providing more efficient delivery in response, requiring less total generation to provide a similar amount of end-point electricity. 
- From a standpoint of reducing emissions, it would be interesting to check for correlations in the fuel-mix makeup during low- and high-generation days/times, to see if there is any trend relating to how renewables are used compared to how much total power is demanded in a given period:
    - If high-generation periods trended toward fossil-fuels, that may indicate a limit in renewable energy capacity, or ability to supply.
    - If periods with a rapidly increasing power generation trended particular fuel types, that may indicate an inability to quickly deploy electricity from those types.
    - It would be interesting to see if renewable usage ever spikes, or if it tracks power generation in a well-correlated and predictable fashion. If there are spikes of renewable energy that seem to indicate a lack of available fossil fuels, this may indicate a built-in contractual bias for fossil fuel sources, as it would imply that renewables were available until that point, but were not being used. 
- As the emissions heatmap tends to track the power generation heatmap fairly well, I wonder, if there is the same general makeup of power generation by fuel type regardless of power generated, why can't we bias toward renewable sources during periods of lower demand?
- It may be interesting to look at day-ahead predictions of usage, which go into the predictions of demand that lead to the calculation of the day-ahead locational based marginal price (LBMP), to spot periods of time that show unexpected levels of generation, and see if there is a strong trend of certain fuel types for supplying additional power during periods of unexpected demand. It may be possible to identify what types of fuel are used more as a back-up vs what's normally on-line.
    - If renewables were lacking in this circumstance, this could also make the argument for more battery usage in renewables, not only to store energy to better supply power during those high-demand periods, but also to perform energy arbitrage on the market in a more predictable and successful fashion, further decreasing the cost. 
- The heatmaps could be expressed, instead of through absolute power generation, as power generation relative to the average, to better see what days are particularly out of character for the month, and try to identify predictable events.
