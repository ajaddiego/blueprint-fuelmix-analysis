import pandas as pd
import numpy as np
import sys, os
import time
import datetime as dt
from matplotlib import pyplot as plt
import seaborn as sns
from os.path import dirname

####################
# Helper Functions #
####################

def format_fuelmix_data(fuelmix_data):
    """"Adjust columns and types of fuelmix data for further manipulation."""

    # all time zones are EDT
    fuelmix_data.drop(['Time Zone'], axis=1, inplace=True)

    # extract specific datetime info for selection / comparison
    fuelmix_data['Date'] = fuelmix_data['Time Stamp'].apply(lambda x: x.date())
    fuelmix_data['Time'] = fuelmix_data['Time Stamp'].apply(lambda x: x.time())
    fuelmix_data['Day']  = fuelmix_data['Time Stamp'].apply(lambda x: x.day)
    fuelmix_data['Hour'] = fuelmix_data['Time Stamp'].apply(lambda x: x.hour)
    fuelmix_data['Minute'] = fuelmix_data['Time Stamp'].apply(lambda x: x.minute)
    fuelmix_data['Second'] = fuelmix_data['Time Stamp'].apply(lambda x: x.second)

    # convert MW to kW
    fuelmix_data['Gen kW'] = fuelmix_data['Gen MW']*1000

    return fuelmix_data

def fix_time_intervals(fuelmix_data):
    """Standardizes data so that all rows represent 5m intervals of time.

    Most of the data is in regular 5m time intervals. This function identifies
    all missing 5m time intervals for each day in the dataset, and interpolates
    them by copying the data from the closest following 5m interval. The function
    then removes all rows that have irregular timestamps to ready the data for 
    feature calculation and plotting. 

    NOTE: The function does not handle consecutive missing intervals, or missing
    intervals at the very beginning of the day, because these circumstances do 
    not exist in the given dataset. However, it would have to be adapted for such
    situations in the event of generalizing beyond the month of August 2019. 

    Keyword arguments:
    fuelmix_data -- the 'fuelmix_df' dataframe created for this analysis, 
        formatted through line 361.

    Returns the fuelmix_data dataframe with completely standardized timestamps.
    """

    # what times should exist for every date?
    all_times = pd.date_range("2021-01-01 00:05:00", freq="5min", periods=12*24-1)
    all_times = pd.Series(all_times).apply(lambda x: x.time())

    # find missing 5m interval timestamps and fill with data from next good timestamp
    # (to generalize for any month, would have to have a check for if next idx didnt
    #  exist or was in the following day, but its fine for august 2019 data)
    for day in range(1,32):

        # isolate day in question
        fuelmix_day = fuelmix_data.loc[fuelmix_data['Day'] == day]

        # iterate over all possible 5m intervals to check for existence on given day
        for ts in all_times:

            # store index of next regular timestamp in case of missing 5m interval
            ts_next_idx = all_times[all_times == ts].index[0] + 1

            # if a 5m interval timestamp is missing, create it and fill with data 
            # from the next valid 5m interval timestamp in the given day 
            if ts not in set(fuelmix_day['Time']):

                # grab the index of the next regular time in the fuelmix dataframe
                next_ts = all_times[ts_next_idx]
                fuelmix_idx = (fuelmix_day['Time'] == next_ts).idxmax()

                # copy rows from the next 5m interval timestamp to insert in place of
                # the missing time, and edit time to act as the missing 5m interval
                new_rows = fuelmix_data.iloc[fuelmix_idx:fuelmix_idx+7, :].copy()
                new_rows['Time Stamp'] = new_rows['Time Stamp'].apply(
                                            lambda x: x - dt.timedelta(minutes=5))
                new_rows['Time'] = new_rows['Time Stamp'].apply(lambda x: x.time())

                # insert new, regular-timestamped rows into proper dataframe location
                fuelmix_data = pd.concat([fuelmix_data.iloc[:fuelmix_idx], new_rows, 
                                          fuelmix_data.iloc[fuelmix_idx:]], ignore_index=True)

                # recalculate fuelmix_day df based on new indexing due to added rows
                # (required if there are >1 missing times on a given day, as the
                #  fuelmix_day indexes are not reset until the outer loop advances)
                fuelmix_day = fuelmix_data.loc[fuelmix_data['Day'] == day]
                
                # print(day, ts, next_ts, fuelmix_idx)

    # remove all invalid timestamps
    fuelmix_data = fuelmix_data[fuelmix_data['Time'].isin(all_times)]

    # NB: fuelmix_data now contains only timestamps for regular 5m intervals
    return fuelmix_data

def compute_kWh(fuelmix_data):
    """Constructs time differentials and computes kWh from MW for each row.
    
    This function constructs and corrects time differentials for the fuelmix
    data, given standardized timestamps. It then calculates kWh from MW using
    these time differentials to prepare for the emissions calculation to follow.
    
    Keyword arguments:
    fuelmix_data -- the 'fuelmix_df' dataframe created for this analysis, 
        formatted and standardized through line 370.

    Returns the fuelmix_data dataframe with time differentials and kWh calculated.
    """

    # construct 'previous timestamp' column for determining length of time window
    timestamps = fuelmix_data['Time Stamp'].drop_duplicates().reset_index(drop=True)
    timestamps_prev = pd.concat([pd.Series(['2019-08-01 00:00:00']),timestamps[:-1]], 
                                ignore_index=True)

    # dict to map previous timestamp onto current time stamp in full fuelmix dataframe
    prevtime_map = dict(zip(timestamps,timestamps_prev))

    # map previous timestamp onto current, and compute time differential
    fuelmix_data['Prev Time Stamp'] = fuelmix_data['Time Stamp'].map(prevtime_map)
    fuelmix_data['timediff'] = fuelmix_data['Time Stamp'] - fuelmix_data['Prev Time Stamp']

    # adjust for 00:05:00 time stamp which will show a false differential of 10m via
    # the above method due to all day-ending timestamps being 23:55:00 (00:05 - 23:55)
    fuelmix_data.loc[fuelmix_data['Time']==dt.time(0,5),'timediff'] = dt.timedelta(minutes=5)

    # calculate kWh based on correct, standardized 5-minute time differentials
    fuelmix_data['timediff_hrs'] = fuelmix_data['timediff'] / dt.timedelta(hours=1)
    fuelmix_data['Gen kWh'] = fuelmix_data['Gen kW']*fuelmix_data['timediff_hrs']

    return fuelmix_data

def determine_average_and_typical_days(fuelmix_data):
    """ Determines the average and typical days for NYISO data in August 2019.

    This function takes in the fuelmix data for August 2019 and computes the 
    average day (by averaging across days for each timestamp), the standard
    error of the average day calculation, and the typical day. The typical day
    is computed by taking the day with the smallest sum-of-squared-differences
    when compared to the average day. 

    Keyword arguments:
    fuelmix_data -- the 'fuelmix_df' dataframe created for this analysis, formatted 
        and standardized with features calculated and added through line 384.
    
    Returns dataframes representing the average day, average standard error, 
    typical day, and the day-number of the calculated typical day.
    """

    ##################################################
    # Determine the average day, with standard error #
    ##################################################

    # format data to sum MW, kWh, kgCO2e across fuel categories for a given datetime
    fuelmix_totals = fuelmix_data.groupby(['Time Stamp'])[['Gen MW','Gen kWh','kgCO2e']]\
                                 .sum()\
                                 .reset_index()

    # add helper columns for ease of use
    fuelmix_totals['Time'] = fuelmix_totals['Time Stamp'].dt.strftime('%H:%M')
    fuelmix_totals['Day']  = fuelmix_totals['Time Stamp'].apply(lambda x: x.day)

    # calculate average day (mean) NYISO total generation for the given dataset
    average_day = fuelmix_totals.groupby(['Time'])\
                                .mean()[['Gen MW','kgCO2e']]\
                                .reset_index()

    # calculate the standard error of the mean for the average day
    avgday_sem = fuelmix_totals.groupby(['Time'])\
                                .sem()[['Gen MW','kgCO2e']]\
                                .reset_index()

    #####################################################################
    # Use sum-of-squared-differences (SSD) to determine the typical day #
    #####################################################################

    # split up the timestamped generation totals by day
    totals_by_day = fuelmix_totals.groupby(['Day'])

    # initialize values for the SSD minimization procedure
    avgday_MW = average_day['Gen MW'].values
    day_data = totals_by_day.get_group(1)
    minimum_ssd = np.sum((day_data['Gen MW'].values - avgday_MW)**2)
    typical_day = np.zeros(shape=287)

    # find the day in august that minimizes the SSD between itself and the average
    for day in range(1,32):

        # determine power generation for the given day and compute SSD
        day_data = totals_by_day.get_group(day)[['Time','Gen MW','kgCO2e']]
        ssd = np.sum((day_data['Gen MW'].values - avgday_MW)**2)

        # save the day information if it represents a new minimum SSD
        if ssd < minimum_ssd:
            typical_day_num = day
            typical_day = day_data.reset_index(drop=True)
            minimum_ssd = ssd

            # print(f'New Typical: Day {min_day} with {minimum_ssd}')

    return average_day, avgday_sem, typical_day, typical_day_num

def plot_NYISO_total_curves(avg_day, avgday_sem, typical_day, plot_type):
    """Plot the total emissions or power for the typical vs average days.
        
    Plots curves of the typical vs average days for either power or emissions 
    data from the NYISO for August 2019 and saves the plot to a .png file in 
    the 'Output' sub-directory.

    Keyword arguments:
    avg_day -- the average day dataframe calculated on line 393.
    avgday_sem -- the average day standard error of the mean dataframe
        calculated on line 393.
    typical_day -- the typical day dataframe calculated on line 393.
    plot_type -- either 'Gen MW' for power, or 'kgCO2e' for emissions.

    Returns nothing.
    """

    # get label string from given plot type
    label_dict = {'Gen MW':'Power Generation (MW)',
                  'kgCO2e':'CO2 Emissions (kg)'}
    label_str = label_dict.get(plot_type)

    # set up common x-axis
    time_axis = avg_day['Time']
    
    # initiate plot objects
    fig, ax = plt.subplots(figsize=(10,10))

    # increase the number of auto-created yticks
    plt.locator_params(axis='y', nbins=10)

    # plot total curves
    plt.plot(time_axis, typical_day[plot_type], label='Typical Day')
    plt.plot(time_axis, avg_day[plot_type], label='Average Day')

    # adjust axes labels and title
    ax.set_title(f'NYISO {label_str}: August 2019',pad=15)
    ax.set_xlabel('Time',labelpad=10)
    ax.set_ylabel(label_str,labelpad=15)

    # show only every 20th timestamp to avoid over-crowding
    ax.set_xticks(time_axis[::20])
    
    # show axes grid in the background
    ax.grid(alpha=0.4, linestyle='-', color='lightgray')

    # plot average day error bars
    plt.errorbar(time_axis, avg_day[plot_type],
                 yerr=[2*i for i in avgday_sem[plot_type]],
                 linestyle='', color='bisque',
                 alpha=0.5)

    # show legend
    plt.legend()

    # format output path string
    output_label = label_str.replace(" ","_").replace("(","").replace(")","")
    output_title = f'NYISO_total_{output_label}_curves.png'
    output_path = os.path.join(plot_dir, output_title)

    # save plot
    plt.savefig(output_path)
    plt.close('all')

    return

def plot_heatmap(fuelmix_data, plot_type):
    """Plot a heatmap of power or emissions data for the NYISO for August 2019.
        
    Plots a heatmap of either power or emissions data from the NYISO for August 
    2019 and saves the plot to a .png file in the 'Output' sub-directory.

    Keyword arguments:
    fuelmix_data -- the specific dataframe created for this analysis, with
        formatting, standardization, and feature calculations completed through
        line 384.
    plot_type -- either 'Gen MW' for power, or 'kgCO2e' for emissions.

    Returns nothing.
    """

    # format data to sum MW, kWh, kgCO2e across fuel categories for a given datetime
    fuelmix_totals = fuelmix_data.groupby(['Time Stamp'])[['Gen MW','Gen kWh','kgCO2e']]\
                                 .sum()\
                                 .reset_index()

    # add helper columns for ease of use
    fuelmix_totals['Time'] = fuelmix_totals['Time Stamp'].dt.strftime('%H:%M')
    fuelmix_totals['Day']  = fuelmix_totals['Time Stamp'].apply(lambda x: x.day)

    # get label string from given plot type
    label_dict = {'Gen MW':'Power Generation (MW)',
                  'kgCO2e':'CO2 Emissions (kg)'}
    label_str = label_dict.get(plot_type)

    # create dataframe containing aggregate data in proper 2d format for heatmap
    fuelmix_pivot = pd.pivot_table(fuelmix_totals,
                                 index = ['Day'],
                               columns = ['Time'],
                               values  = ['Gen MW','kgCO2e'])

    # extract power or emissions data from pivot table
    heatmap_data = fuelmix_pivot.loc[:,[plot_type]]

    # reverse columns and flatten multi-index for plot presentation
    heatmap_data = heatmap_data[heatmap_data.columns[::-1]]
    heatmap_data.columns = heatmap_data.columns.get_level_values(1)

    # reset tick locator parameter to 'auto' default
    plt.locator_params(axis='y', nbins='auto')
    plt.locator_params(axis='x', nbins='auto')

    # plot the heatmap
    plt.figure(figsize=(13,11))
    ax = sns.heatmap(heatmap_data.T)

    # adjust plot parameters
    ax.set_ylabel('Time')
    ax.set_title(f'NYISO {label_str}: August 2019')
    
    # format output path string
    output_label = label_str.replace(" ","_").replace("(","").replace(")","")
    output_title = f'NYISO_{output_label}_heatmap.png'
    output_path = os.path.join(plot_dir, output_title)

    # save plot
    plt.savefig(output_path)
    plt.close('all')

    return


#########################################
# Load in fuel-mix data for august 2019 #
#########################################
print('Loading data...')

# establish local directory filepaths for the working instance
script_loc = dirname(os.path.abspath(__file__))
parent_dir = dirname(script_loc)
code_dir = os.path.join(parent_dir, 'Code')
data_dir = os.path.join(parent_dir, 'Data')
plot_dir = os.path.join(parent_dir, 'Output')

# establish fuelmix data paths
fuelmix_dir = os.path.join(data_dir,'20190801rtfuelmix_csv/')
fuelmix_files = [os.path.join(fuelmix_dir,f'201908{str(day).zfill(2)}rtfuelmix.csv') 
                    for day in range(1,32)]

# load fuelmix data into dataframe and lightly adjust columns for ease of use
fuelmix_df = pd.concat((pd.read_csv(f, parse_dates=['Time Stamp']) for f in fuelmix_files))
fuelmix_df = format_fuelmix_data(fuelmix_df)


###################################################################
# Clean up time intervals and calculate kWh and emissions (kgCO2) #
###################################################################
print('Cleaning up time intervals and calculating kWh and emissions...')

# standardize to 5m time intervals and use them to compute kWh from MW + timediff
fuelmix_df = fix_time_intervals(fuelmix_df)
fuelmix_df = compute_kWh(fuelmix_df)

# establish multipliers (kgCO2e/kWh) for calculating kgCO2 emissions
fuelmap = {'Dual Fuel'   : 0.444,
           'Natural Gas' : 0.4626,
           'Other Fossil Fuels' : 0.935,
           'Other Renewables'   : 0.256,
           'Nuclear' : 0,
           'Wind'    : 0,
           'Hydro'   : 0}

# calculate kgCO2e per row of power generation
fuelmix_df['kgCO2e_coef'] = fuelmix_df['Fuel Category'].map(fuelmap)
fuelmix_df['kgCO2e'] = fuelmix_df['Gen kWh']*fuelmix_df['kgCO2e_coef']


##########################################################
# Calculate average and typical days, and produce plots  #
##########################################################
print('Calculating the average and typical days...')

# calculate the average and typical days, including standard error for avg day
avg_day, avg_sem, typ_day, typ_num = determine_average_and_typical_days(fuelmix_df)

print('Plotting average vs typical curves...')

# plot NYISO total generation / emissions for average against typical days
plot_NYISO_total_curves(avg_day, avg_sem, typ_day, 'Gen MW')
plot_NYISO_total_curves(avg_day, avg_sem, typ_day, 'kgCO2e')

print('Plotting heatmaps...')

# plot heatmaps (including extra heatmap of emissions)
plot_heatmap(fuelmix_df,'Gen MW')
plot_heatmap(fuelmix_df,'kgCO2e')

print('Complete.')

